//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop
USERES("SimCall3000.res");
USEFORM("MAIN.cpp", form_principal);
USEUNIT("Datos\AGENTE.cpp");
USEUNIT("Datos\Listas\Lista Agente\Info_agente.cpp");
USEUNIT("Datos\Listas\Lista Agente\Nodo_agente.cpp");
USEUNIT("Datos\Listas\Lista Agente\Lista_agentes.cpp");
USEUNIT("Datos\Listas\Lista Clientes\Info_cliente.cpp");
USEUNIT("Datos\CLIENTE.cpp");
USEUNIT("Datos\LLAMADA.cpp");
USEUNIT("Datos\Listas\Lista Clientes\Nodo_cliente.cpp");
USEUNIT("Datos\Listas\Lista Clientes\Lista_clientes.cpp");
USEUNIT("Datos\Listas\Lista Llamadas\Info_llamada.cpp");
USEUNIT("Datos\Listas\Lista Llamadas\Nodo_llamada.cpp");
USEUNIT("Datos\Listas\Lista Llamadas\Lista_llamadas.cpp");
USEUNIT("Datos\Listas\Cola Prioridades\Nodo_cola_prioridades.cpp");
USEUNIT("Datos\Listas\Cola Prioridades\Cola_prio.cpp");
USEUNIT("Datos\Listas\Cola Prioridades\Cola_prioridades.cpp");
USEUNIT("Datos\Listas\Lista Servicios\Info_servicios.cpp");
USEUNIT("Datos\Listas\Lista Servicios\Nodo_servicios.cpp");
USEUNIT("Datos\Listas\Lista Servicios\Lista_servicios.cpp");
USEFORM("Admin.cpp", form_admin);
USEFORM("SPQR.cpp", form_spqr);
USEUNIT("Gestor\Gestor.cpp");
USEFORM("LogEmp.cpp", form_log);
USEFORM("Reportes.cpp", form_reportes);
USEFORM("RealizarLlamada.cpp", form_llamada);
//---------------------------------------------------------------------------
WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
    try
    {
        Application->Initialize();
        Application->CreateForm(__classid(Tform_principal), &form_principal);
        Application->CreateForm(__classid(Tform_admin), &form_admin);
        Application->CreateForm(__classid(Tform_spqr), &form_spqr);
        Application->CreateForm(__classid(Tform_log), &form_log);
        Application->CreateForm(__classid(Tform_reportes), &form_reportes);
        Application->CreateForm(__classid(Tform_llamada), &form_llamada);
        Application->Run();
    }
    catch (Exception &exception)
    {
        Application->ShowException(&exception);
    }
    return 0;
}
//---------------------------------------------------------------------------
