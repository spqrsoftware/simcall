//---------------------------------------------------------------------------
#ifndef ReportesH
#define ReportesH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
#include <ComCtrls.hpp>
//---------------------------------------------------------------------------
class Tform_reportes : public TForm
{
__published:	// IDE-managed Components
    TImage *Image1;
    TImage *Image2;
    TRichEdit *area_agentes;
    TLabel *Label1;
    TButton *Button1;
    TComboBox *box_agentes_servicios;
    TLabel *Label2;
    TLabel *Label3;
    TRichEdit *area_agentes_servicios;
    TImage *Image3;
    TRichEdit *area_llamadas_perdidas;
    TLabel *Label4;
    TComboBox *box_colas;
    TLabel *Label8;
    TRichEdit *area_llamdas_atendidas;
    TRichEdit *area_llamadas_esperando;
    TLabel *Label9;
    TLabel *Label10;
    TBevel *Bevel1;
    TBevel *Bevel2;
    TBevel *Bevel3;
    void __fastcall Button1Click(TObject *Sender);
private:	// User declarations
public:		// User declarations
    __fastcall Tform_reportes(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE Tform_reportes *form_reportes;
//---------------------------------------------------------------------------
#endif
