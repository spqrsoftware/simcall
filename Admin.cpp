//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include "Admin.h"
#include "Info_agente.h"
#include "AGENTE.h"
#include "Gestor.h"
#include "SPQR.h"
#include "Reportes.h"
#include "LogEmp.h"
#include "RealizarLlamada.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
Tform_admin *form_admin;
//---------------------------------------------------------------------------
__fastcall Tform_admin::Tform_admin(TComponent* Owner)
    : TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall Tform_admin::SalirdelModoAdministrador1Click(TObject *Sender)
{
Close();
}
//---------------------------------------------------------------------------
void __fastcall Tform_admin::Agentes1Click(TObject *Sender){
        panel_ingreso_ser -> Hide();
        panel_clientes -> Hide();
    if( panel_agentes -> Visible == false ){
        panel_agentes -> Show();
    }else{
        panel_agentes -> Hide();
        txt_nom_emp -> Clear();
        txt_ap1_emp -> Clear();
        txt_ap2_emp -> Clear();
        txt_puesto_emp -> Clear();
        barratexto_ingreso_emp -> Text = "Todo Normal";
        area_ingreso_emp -> Text = "--Esperando--";
    }   //fin del if...else

}
//---------------------------------------------------------------------------

void __fastcall Tform_admin::btn_ingresar_empleadoClick(TObject *Sender){

    Gestor :: contAgentes++;
    InfoAgente * temp = new InfoAgente( new Agente(Gestor:: contAgentes,
        txt_nom_emp -> Text.c_str(), txt_ap1_emp -> Text.c_str(),
        txt_ap2_emp -> Text.c_str(), txt_puesto_emp -> Text.c_str() ) );
    Gestor :: insertarAgente( temp );
    area_ingreso_emp -> Text = Gestor :: buscarAgente( Gestor :: contAgentes ) -> aString().c_str();
    barratexto_ingreso_emp -> Text = "Se ha inscrito un nuevo empleado";
    txt_nom_emp -> Clear();
    txt_ap1_emp -> Clear();
    txt_ap2_emp -> Clear();
    txt_puesto_emp -> Clear();

}
//---------------------------------------------------------------------------

void __fastcall Tform_admin::btn_limpiar_pantalla_empleadoClick(TObject *Sender){

    txt_nom_emp -> Clear();
    txt_ap1_emp -> Clear();
    txt_ap2_emp -> Clear();
    txt_puesto_emp -> Clear();
    barratexto_ingreso_emp -> Text = "Se ha limpiado la pantalla";
    area_ingreso_emp -> Text = "";

}
//---------------------------------------------------------------------------

void __fastcall Tform_admin::FormClose(TObject *Sender, TCloseAction &Action){

panel_clientes -> Hide();
    panel_ingreso_ser -> Hide();
    panel_agentes -> Hide();
    txt_nom_emp -> Clear();
    txt_ap1_emp -> Clear();
    txt_ap2_emp -> Clear();
    txt_puesto_emp -> Clear();
    barratexto_ingreso_emp -> Text = "Todo Normal";
    area_ingreso_emp -> Text = "--Esperando--";

}
//---------------------------------------------------------------------------

void __fastcall Tform_admin::Servicios1Click(TObject *Sender){
    panel_clientes -> Hide();
    panel_agentes -> Hide();
    txt_nom_emp -> Clear();
    txt_ap1_emp -> Clear();
    txt_ap2_emp -> Clear();
    txt_puesto_emp -> Clear();
    barratexto_ingreso_emp -> Text = "Todo Normal";
    area_ingreso_emp -> Text = "--Esperando--";
    if( panel_ingreso_ser -> Visible == false ){
        panel_ingreso_ser -> Show();
    }else{
        panel_ingreso_ser -> Hide();

    }   //fin del if...else

}
//---------------------------------------------------------------------------

void __fastcall Tform_admin::LogdeEmpleados1Click(TObject *Sender)
{
    form_log -> ShowModal();    
}
//---------------------------------------------------------------------------

void __fastcall Tform_admin::Todoslosreportes1Click(TObject *Sender)
{
    form_reportes -> ShowModal();    
}
//---------------------------------------------------------------------------

void __fastcall Tform_admin::AcercadeSPQRSoftware1Click(TObject *Sender)
{
    form_spqr -> ShowModal();    
}
//---------------------------------------------------------------------------

void __fastcall Tform_admin::btn_ingresar_serClick(TObject *Sender){
    string msn = txt_ingresar_ser -> Text.c_str();
    Gestor :: contServicios++;
    InfoServicios * temp = new InfoServicios( msn, Gestor :: contServicios );
    Gestor :: insertarServicio( temp );
    msn = Gestor :: buscarServicio( msn ) -> aString();
    area_servicio -> Text = msn.c_str();


}
//---------------------------------------------------------------------------

void __fastcall Tform_admin::Button1Click(TObject *Sender){
    Gestor :: contClientes++;
    InfoCliente * temp = new InfoCliente(
        new Cliente( Gestor :: contClientes, txt_nom -> Text.c_str(),
        txt_ap1 -> Text.c_str(), txt_ap2 -> Text.c_str(), txt_tel -> Text.c_str()) );
    Gestor :: insertarCliente( temp );
    Gestor :: desplegarClientes( area );

}
//---------------------------------------------------------------------------

void __fastcall Tform_admin::Clientes1Click(TObject *Sender)
{
    panel_ingreso_ser -> Hide();
    panel_agentes -> Hide();
    if( panel_clientes -> Visible == false ){
        panel_clientes -> Show();
    }else{
        panel_clientes -> Hide();
    }   //fin del if...else
}
//---------------------------------------------------------------------------

void __fastcall Tform_admin::Ingresarllamadas1Click(TObject *Sender)
{
    form_llamada -> ShowModal();    
}
//---------------------------------------------------------------------------

