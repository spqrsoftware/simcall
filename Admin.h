//---------------------------------------------------------------------------
#ifndef AdminH
#define AdminH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
#include <Menus.hpp>
#include <ComCtrls.hpp>
//---------------------------------------------------------------------------
class Tform_admin : public TForm
{
__published:	// IDE-managed Components
    TMainMenu *MainMenu1;
    TMenuItem *Principal1;
    TMenuItem *AcercadeSPQRSoftware1;
    TMenuItem *SalirdelModoAdministrador1;
    TMenuItem *Registro1;
    TMenuItem *Clientes1;
    TMenuItem *Agentes1;
    TMenuItem *Servicios1;
    TMenuItem *Servicios3;
    TMenuItem *LogdeEmpleados1;
    TMenuItem *Reportes1;
    TImage *Image2;
    TImage *Image3;
    TImage *Image1;
    TPanel *panel_agentes;
    TImage *Image4;
    TEdit *txt_nom_emp;
    TEdit *txt_ap1_emp;
    TEdit *txt_ap2_emp;
    TEdit *txt_puesto_emp;
    TLabel *Label1;
    TLabel *Label2;
    TLabel *Label3;
    TLabel *Label4;
    TRichEdit *area_ingreso_emp;
    TRichEdit *barratexto_ingreso_emp;
    TButton *btn_ingresar_empleado;
    TButton *btn_limpiar_pantalla_empleado;
    TPanel *panel_ingreso_ser;
    TImage *Image5;
    TEdit *txt_ingresar_ser;
    TLabel *Label5;
    TRichEdit *area_servicio;
    TEdit *txt_elim_ser;
    TLabel *Label6;
    TButton *btn_ingresar_ser;
    TButton *btn_elim_ser;
    TMenuItem *Todoslosreportes1;
    TPanel *panel_clientes;
    TImage *Image6;
    TEdit *txt_nom;
    TEdit *txt_ap1;
    TEdit *txt_ap2;
    TEdit *txt_tel;
    TLabel *Label7;
    TLabel *Label8;
    TLabel *Label9;
    TLabel *Label10;
    TRichEdit *area;
    TButton *Button1;
    TMenuItem *Ingresarllamadas1;
    void __fastcall SalirdelModoAdministrador1Click(TObject *Sender);
    void __fastcall Agentes1Click(TObject *Sender);
    void __fastcall btn_ingresar_empleadoClick(TObject *Sender);
    void __fastcall btn_limpiar_pantalla_empleadoClick(TObject *Sender);
    void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
    void __fastcall Servicios1Click(TObject *Sender);
    void __fastcall LogdeEmpleados1Click(TObject *Sender);
    void __fastcall Todoslosreportes1Click(TObject *Sender);
    void __fastcall AcercadeSPQRSoftware1Click(TObject *Sender);
    void __fastcall btn_ingresar_serClick(TObject *Sender);
    void __fastcall Button1Click(TObject *Sender);
    void __fastcall Clientes1Click(TObject *Sender);
    void __fastcall Ingresarllamadas1Click(TObject *Sender);
private:	// User declarations
public:		// User declarations
    __fastcall Tform_admin(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE Tform_admin *form_admin;
//---------------------------------------------------------------------------
#endif
