//---------------------------------------------------------------------------
#ifndef SPQRH
#define SPQRH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
//---------------------------------------------------------------------------
class Tform_spqr : public TForm
{
__published:	// IDE-managed Components
    TImage *Image1;
    TImage *Image2;
private:	// User declarations
public:		// User declarations
    __fastcall Tform_spqr(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE Tform_spqr *form_spqr;
//---------------------------------------------------------------------------
#endif
