//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include "Gestor.h"

ListaLlamadas *Gestor :: llamadasPerdidas = NULL;
ListaAgentes *Gestor :: agentes = NULL;
ListaServicios *Gestor :: servicios = NULL;
ListaClientes *Gestor :: clientes = NULL;
int Gestor :: contAgentes = 0;
int Gestor :: contServicios = 0;
int Gestor :: contClientes = 0;
InfoServicios *Gestor :: servicio = NULL;


Gestor :: Gestor(){}

/*M�todo que instancia los atributos del gestor. S�LO debe ser usado una
vez en todo el programa*/
void Gestor :: instanciar(){
    Gestor :: llamadasPerdidas = new ListaLlamadas();
    Gestor :: agentes = new ListaAgentes();
    Gestor :: servicios = new ListaServicios();
    Gestor :: clientes = new ListaClientes();
    Gestor :: contServicios = 0;
    Gestor :: contAgentes = 0;
    Gestor :: contClientes = 0;
}   //fin del m�todo instanciar

//******************  M�todos que involucran agentes  ************************//

Agente * Gestor :: buscarAgente( int cod ){
    return Gestor :: agentes -> buscar( cod );
}   //fin del m�todo buscarAgente

/*M�todo que inserta un agente a la lista global de agentes*/
void Gestor :: insertarAgente(InfoAgente * temp){
    Gestor :: agentes -> insertar( temp );
}   //fin del insertarAgente

void Gestor :: borrarAgente(int cod){
    Gestor :: agentes -> eliminar( cod );

}   //fin del m�todo borrarAgente

void Gestor :: desplegarAgentes( TRichEdit * area ){
    Gestor :: agentes -> desplegar( area );
}   //fin del m�todo desplegarAgentes

void Gestor :: asignarAgentes( TComboBox * box ){
    Gestor :: agentes -> asignarAgentes( box );
}   //fin del m�todo asignarAgentes

string Gestor::loguearAgente(int cod, string cola){
    Agente *agen = Gestor::buscarAgente(cod);
    if (!agen->getEstado()){
        Gestor::abrirServicio(cola);
        if (Gestor::getServicio() == NULL){
            return "El comboBox no sirve";
        }else{
            getServicio()->getAgentes()->insertar(new InfoAgente(agen));
        }
        agen -> cambiarEstado();
        agen -> setConexionCola( cola );
        return "El agente ha sido ingresado en el servicio";
    }else{
        return "El agente ya esta activo";
    }
} 

string Gestor::desloguearAgente(int cod){
    Agente *agen = Gestor::buscarAgente(cod);
    if (agen->getEstado()){
        Gestor::abrirServicio(agen->getConexionCola());
        if (Gestor::getServicio() == NULL){
            return "El comboBox no sirve";
        }else{
            getServicio()->getAgentes()->eliminar(cod);
        }
        agen -> cambiarEstado();
        agen -> setConexionCola( "" );
        return "El agente ha sido desconectado del servicio";
    }else{
        return "El agente no esta activo";
    }
}

string Gestor::contestar(int cod, int h, int m, int s){
    Agente *agen = Gestor::buscarAgente(cod);
    if (!agen->getEstadoConexion()){
        Gestor::abrirServicio(agen->getConexionCola());
        agen->setLlamada(getServicio()->getCola()->atenderLlamada());
        agen -> setEstadoConexion(true);
        agen -> getLlamada()->getLlamada()->setHoraAtendido(h,m,s);
        return "El agente esta atendido una llamada";
    }else{
        return "El agente ya esta ocupado";
    }
}

string Gestor::colgar(int cod, int h, int m, int s){
    Agente *agen = Gestor::buscarAgente(cod);
    if (agen->getEstadoConexion()){
        Gestor::abrirServicio(agen->getConexionCola());
        agen -> getLlamada()->getLlamada()->setHoraFinLlamada(h,m,s);
        getServicio()->getLlamadasAtendidas()->insertarAlInicio(agen -> getLlamada());
        agen -> setEstadoConexion(false);
        agen->setLlamada(NULL);
        return "El agente ha finalizado una llamada";
    }else{
        return "El agente esta desocupado";
    }
}

string Gestor::perderLlamada(int cod, int h, int m, int s, int i){
    try{
        Agente *agen = Gestor::buscarAgente(cod);
        Gestor::abrirServicio(agen->getConexionCola());
        agen->setLlamada( getServicio()->getCola()->perderLlamada(i));
        agen->getLlamada()->getLlamada()->setHoraFinLlamada(h,m,s);
        Gestor::llamadasPerdidas->insertarAlInicio(agen->getLlamada());
        return "Se ha perdido una llamada";
    }catch (...){
        return "no se pudo \"perder\" la llamada";
    }
}

void Gestor :: insertarCliente( InfoCliente * info ){
    Gestor :: clientes -> insertarAlInicio( info );
}   //fin del m�todo insertarCliente

void Gestor :: desplegarClientes( TRichEdit * area ){
    Gestor :: clientes -> despliegue( area );
}   //fin del m�todo desplegarClientes


//******************  M�todos relacionados con los servicios  ****************//
void Gestor :: asignarServicios( TComboBox * box ){
    Gestor :: servicios -> desplegar( box );
} //fin del m�todo asignarServicios

void Gestor :: insertarServicio( InfoServicios * temp ){
    Gestor :: servicios -> insertar( temp );
}   //fin del m�todo insertarServicio

void Gestor :: borrarServicio( string c ){
    Gestor :: servicios -> eliminar( c );
}   //fin del m�todo borrarServicio

InfoServicios * Gestor :: buscarServicio( string c ){
    return Gestor :: servicios -> buscar( c );
}   //fin del m�todo buscarSerivio

void Gestor :: abrirServicio( string nom ){
    Gestor :: servicio = Gestor :: servicios -> buscar( nom );
}   //fin del m�todo abrirServicio

InfoServicios * Gestor :: getServicio(){
    return Gestor :: servicio;
}   //fin del m�todo getServico


#pragma package(smart_init)
