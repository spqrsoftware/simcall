//---------------------------------------------------------------------------
#ifndef GestorH
#define GestorH

#include "Lista_llamadas.h"
#include "Lista_clientes.h"
#include "Lista_servicios.h"
#include "Lista_agentes.h"
#include "Info_servicios.h"


class Gestor{
    private:    

        Gestor();
        static ListaLlamadas *llamadasPerdidas;
        static ListaAgentes *agentes;
        static ListaServicios *servicios;
        static ListaClientes *clientes;
        static InfoServicios * servicio;


    public:
        static int contAgentes;
        static int contServicios;
        static int contClientes;

        static void instanciar();

        static void insertarCliente( );
        static void borrarCliente();

//*************************  M�todos para agentes  ***************************//
        static void insertarAgente( InfoAgente * );
        static void borrarAgente(int);
        static void desplegarAgentes( TRichEdit * );
        static Agente * buscarAgente(int);
        static void asignarAgentes( TComboBox * ); 
        static string loguearAgente(int, string);
        static string desloguearAgente(int);
        static string contestar(int, int, int, int);  
        static string colgar(int, int, int, int);
        static string perderLlamada(int, int, int, int, int);

        
        static void insertarLlamada( InfoLlamada * );
        static void perderLlamada();

        static void insertarServicio( InfoServicios * );
        static void borrarServicio(string);

        static Llamada * atenderLlamada();
        static Cliente * buscarCliente(int);
        static void insertarCliente( InfoCliente * );
        static void desplegarClientes( TRichEdit * );


        static void asignarServicios( TComboBox * );
        static void abrirServicio( string );
        static InfoServicios * getServicio();
        static InfoServicios * buscarServicio( string );
};
//---------------------------------------------------------------------------
#endif
