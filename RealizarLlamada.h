//---------------------------------------------------------------------------
#ifndef RealizarLlamadaH
#define RealizarLlamadaH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
//---------------------------------------------------------------------------
class Tform_llamada : public TForm
{
__published:	// IDE-managed Components
    TImage *Image1;
    TEdit *txt_h;
    TEdit *txt_m;
    TEdit *txt_s;
    TLabel *Label1;
    TLabel *Label2;
    TLabel *Label3;
    TButton *Button1;
    TLabel *Label4;
    TEdit *txt_prio;
    TComboBox *box;
    TLabel *Label5;
    TButton *Button2;void __fastcall Button2Click(TObject *Sender);
    void __fastcall Button1Click(TObject *Sender);
private:	// User declarations
public:		// User declarations
    __fastcall Tform_llamada(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE Tform_llamada *form_llamada;
//---------------------------------------------------------------------------
#endif
