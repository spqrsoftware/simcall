//---------------------------------------------------------------------------
#ifndef LogEmpH
#define LogEmpH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
#include "agente.h"
#include "gestor.h"
#include <ComCtrls.hpp>
//---------------------------------------------------------------------------
class Tform_log : public TForm
{
__published:	// IDE-managed Components
    TImage *Image1;
    TImage *Image2;
    TButton *Button1;
    TButton *Button2;
    TButton *Button3;
    TComboBox *combo_agente;
    TComboBox *combo_servicio;
    TRichEdit *text;
    TLabel *Label1;
    TLabel *Label2;
    TBevel *Bevel1;
    TPanel *panel;
    TImage *Image3;
    TComboBox *combo_agente2;
    TEdit *txt_h;
    TEdit *txt_m;
    TEdit *txt_s;
    TLabel *Label3;
    TLabel *Label4;
    TLabel *Label5;
    TButton *Button4;
    TButton *Button5;
    TComboBox *combo_prio;
    TButton *Colgar;
    void __fastcall Button1Click(TObject *Sender);
    void __fastcall Button2Click(TObject *Sender);
    void __fastcall Button3Click(TObject *Sender);
    void __fastcall Button4Click(TObject *Sender);
    void __fastcall ColgarClick(TObject *Sender);
    void __fastcall Button5Click(TObject *Sender);
private:	// User declarations
public:		// User declarations
    __fastcall Tform_log(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE Tform_log *form_log;
//---------------------------------------------------------------------------
#endif
