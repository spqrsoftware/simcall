//---------------------------------------------------------------------------
#ifndef MAINH
#define MAINH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ComCtrls.hpp>
#include <ExtCtrls.hpp>
#include <Buttons.hpp>
//---------------------------------------------------------------------------
class Tform_principal : public TForm
{
__published:	// IDE-managed Components
    TImage *Image1;
    TImage *Image2;
    TPanel *Panel1;
    TPanel *Panel2;
    TLabel *Label2;
    TLabel *Label3;
    TImage *Image3;
    TImage *Image4;
    TLabel *Label1;
    TBitBtn *BitBtn1;
    TButton *Button1;
    void __fastcall Button1Click(TObject *Sender);
private:	// User declarations
public:		// User declarations
    __fastcall Tform_principal(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE Tform_principal *form_principal;
//---------------------------------------------------------------------------
#endif
