//---------------------------------------------------------------------------
#ifndef Lista_llamadasH
#define Lista_llamadasH
#include "Info_llamada.h"
#include "Nodo_llamada.h"
class ListaLlamadas{
    private:
        NodoLlamada * cab;
        void setCab( NodoLlamada * );

    public:
        NodoLlamada * getCab();
        ListaLlamadas();
        void insertarAlInicio( InfoLlamada * );
        void insertarAlFinal( InfoLlamada * );
        void borrarAlInicio();
        void borrarAlFinal();
        void despliegue( TRichEdit * caja);
};   //fin de la clase ListaSimple
#endif
 