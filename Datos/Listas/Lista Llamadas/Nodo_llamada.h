//---------------------------------------------------------------------------
#ifndef Nodo_llamadaH
#define Nodo_llamadaH
#include "Info_llamada.h"
class NodoLlamada{
    private:
        InfoLlamada * dato;
        NodoLlamada * ant;
        NodoLlamada * sig;
    public:
        NodoLlamada();
        NodoLlamada( InfoLlamada * x );

        void setDato( InfoLlamada * x );
        void setAnt( NodoLlamada * anterior );
        void setSig( NodoLlamada * siguiente );

        InfoLlamada * getDato();
        NodoLlamada * getAnt();
        NodoLlamada * getSig();
        
};  //fin de la clase Nodo
#endif
 