//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include "Nodo_llamada.h"

NodoLlamada :: NodoLlamada(){
    dato = NULL;
    sig = NULL;
}   //fin del constructor

NodoLlamada :: NodoLlamada( InfoLlamada * x ){
    dato = x;
    sig = NULL;
}   //fin del constructor

void NodoLlamada :: setDato( InfoLlamada * x ){
    dato = x;
}

void NodoLlamada :: setSig( NodoLlamada * siguiente ){
    sig = siguiente;
}
InfoLlamada * NodoLlamada :: getDato(){
    return dato;
}

NodoLlamada * NodoLlamada :: getSig(){
    return sig;
}
#pragma package(smart_init)
 