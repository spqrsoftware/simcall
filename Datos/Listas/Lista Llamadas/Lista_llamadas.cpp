//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include "Lista_llamadas.h"

ListaLlamadas :: ListaLlamadas(){
    setCab(NULL);
}

void ListaLlamadas :: setCab( NodoLlamada * jupa){
    cab = jupa;
}   //fin del setCab


//*******************************  P�blicos  *********************************//
NodoLlamada * ListaLlamadas :: getCab(){
    return cab;
}   //fin del getCab

void ListaLlamadas :: insertarAlInicio( InfoLlamada * info ){
    NodoLlamada * temp = new NodoLlamada( info );
    temp -> setSig( getCab() );
    setCab( temp );
}   //fin del m�todo

void ListaLlamadas :: insertarAlFinal( InfoLlamada * info ){
    NodoLlamada * temp = new NodoLlamada( info );
    if( getCab() == NULL ){
        insertarAlInicio( info );
    }else{
        NodoLlamada * iterador = getCab();
        while( iterador -> getSig() != NULL ){
            iterador = iterador -> getSig();
        }   //fin del while
        iterador -> setSig( temp );
    }   //fin del if....else
}   //fin del m�todo

void ListaLlamadas :: borrarAlInicio(){//metodo para borrar al inicio
    NodoLlamada * iterador = getCab();

    if(getCab()!=NULL){//si hay algo en la lista
        if(getCab()->getSig()==NULL){//si solo hay un elemento
            delete cab;
            cab=NULL;
        }// cierra IF si solo hay 1 elemento
        else{//else si hay mas de 1 elemento
            setCab(getCab()->getSig() );
            delete iterador;
            iterador=NULL;
        }//cierra else
    }//cierra IF si lista es vacia
}//fin de metodo eliminar al inicio

void ListaLlamadas :: borrarAlFinal(){//metodo que elimina del final

    if(getCab()!=NULL){//si la lista NO esta vacia
        if(getCab()->getSig()==NULL){//si solo hay 1 elemento
            delete cab;
            cab=NULL;
        }//cierra IF si hay solo 1 elemento
        else{//else si hay mas de 1 elemento
            NodoLlamada * ant = getCab();
            NodoLlamada * borrar = getCab()->getSig();
            while(borrar->getSig()!=NULL){
                ant=ant->getSig();
                borrar=borrar->getSig();

            }//cierra while
            ant->setSig(NULL);
            delete borrar;
            borrar=NULL;
        }//cierra ELSE
    }//cierra if si la lista esta vacia
}//fin metodo borrar al final

void ListaLlamadas ::despliegue( TRichEdit * caja){
    if( cab != NULL ){
        NodoLlamada * iterador = cab;
        char * contacto = new char();
        while( iterador != NULL ){
            contacto = new char();
            strcat( contacto, (iterador -> getDato() -> getLlamada()
                    -> getCliente() -> getTelefono() ).c_str() );
            //caja -> Items -> Add(contacto);

            iterador = iterador -> getSig();
        }   //fin del while
    }   //fin del if
}   //fin del m�todo despliegue
#pragma package(smart_init)
 