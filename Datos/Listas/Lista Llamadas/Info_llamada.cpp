//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include "Info_llamada.h"

InfoLlamada :: InfoLlamada(){
    setLlamada(NULL);
}   //fin del constructor

InfoLlamada :: InfoLlamada( Llamada * x ){
    setLlamada(x);
}   //fin de constructor

void InfoLlamada :: setLlamada( Llamada * x ){
    llamada = x;
}   //fin del setInfo

Llamada * InfoLlamada :: getLlamada(){
    return llamada;
}   //fin del getInfo
#pragma package(smart_init)
 