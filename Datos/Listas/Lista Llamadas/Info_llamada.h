//---------------------------------------------------------------------------
#ifndef Info_llamadaH
#define Info_llamadaH
#include "LLAMADA.h"
class InfoLlamada{
    private:
        Llamada * llamada;
    public:
        InfoLlamada();
        InfoLlamada( Llamada * );
        void setLlamada( Llamada * );
        Llamada * getLlamada();
};   //fin de la clase Info
#endif
 