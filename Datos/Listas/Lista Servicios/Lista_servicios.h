//---------------------------------------------------------------------------
#ifndef Lista_serviciosH
#define Lista_serviciosH
#include "Nodo_servicios.h"
#include "Info_servicios.h"
class ListaServicios{
    private:
        NodoServicios * cab;
        void setCab( NodoServicios * );
        NodoServicios * getCab();
    public:
        ListaServicios();

        void insertar( InfoServicios * );
        void eliminar( string );
        InfoServicios * buscar( string );

        void desplegar( TComboBox * );

};  //fin del la clase ListaServicios
#endif
 