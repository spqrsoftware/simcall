//---------------------------------------------------------------------------
#ifndef Info_serviciosH
#define Info_serviciosH
#include "Lista_agentes.h"
#include "Lista_llamadas.h"
#include "Cola_prioridades.h"

class InfoServicios{
    private:
        ColaPrioridades * cola;
        string nombre;
        ListaAgentes * agentes;
        ListaLlamadas * atendidas;
        int codigo;
    public:
        InfoServicios( string, int );
        ColaPrioridades * getCola();
        
        void setCodigo( int );
        int getCodigo();
        void setNombre( string );
        string getNombre();
        ListaAgentes * getAgentes();
        ListaLlamadas * getLlamadasAtendidas();

        string aString();
};  //fin de la class InfoServicios
#endif
 