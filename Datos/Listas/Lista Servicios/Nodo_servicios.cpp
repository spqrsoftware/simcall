//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include "Nodo_servicios.h"
    NodoServicios :: NodoServicios( InfoServicios * i){
        setSig( NULL );
        setAnt( NULL );
        setInfo( i );
    }   //fin del constructor

    void NodoServicios :: setSig( NodoServicios * sigui){
        sig = sigui;
    }
    void NodoServicios :: setAnt( NodoServicios * ante){
        ant = ante;
    }
    void NodoServicios :: setInfo( InfoServicios * i ){
        info = i;
    }

    NodoServicios * NodoServicios :: getSig(){
        return sig;
    }
    NodoServicios * NodoServicios :: getAnt(){
        return ant;
    }
    InfoServicios * NodoServicios :: getInfo(){
        return info;
    }
#pragma package(smart_init)
 