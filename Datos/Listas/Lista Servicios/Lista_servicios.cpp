//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include "Lista_servicios.h"
    ListaServicios :: ListaServicios(){
        setCab( NULL );
    }

    void ListaServicios :: setCab( NodoServicios * c ){
        cab = c;
    }
    NodoServicios * ListaServicios :: getCab(){
        return cab;
    }
               
    void ListaServicios :: insertar( InfoServicios * i){
        NodoServicios * temp = new NodoServicios( i );
        if( getCab() == NULL ){
            setCab( temp );
        }else{
            temp -> setSig( getCab() );
            getCab() -> setAnt( temp );
            setCab( temp );
        }   //fin del if..else
    }   //fin del m�todo insertar

    void ListaServicios :: eliminar( string nom ){
        NodoServicios * iterador = getCab();
        while( iterador -> getSig() != NULL ){
            if( nom.compare( iterador -> getInfo() -> getNombre()) == 0 ){ //1
                if( iterador == getCab() ){
                    setCab( iterador -> getSig() );
                    getCab() -> setAnt( NULL );
                    delete iterador;
                    iterador = NULL;
                    return;
                }else{
                    iterador -> getAnt() -> setSig( iterador -> getSig() );
                    iterador -> getSig() -> setAnt( iterador -> getAnt() );
                    delete iterador;
                    iterador = NULL;
                    return;
                }   //fin del if...else
            }   //fin del if 1
            iterador = iterador -> getSig();
        }   //fin del while
        if( nom.compare( iterador -> getInfo() -> getNombre()) == 0 ){ //2
          if( iterador == getCab() ){
                delete iterador;
                iterador = NULL;
                setCab( NULL );
                return;
            }else{
                iterador -> getAnt() -> setSig( NULL );
                delete iterador;
                iterador = NULL;
            }   //fin del if...else
        }   //fin del if 2
    }   //fin del m�todo eliminar

    InfoServicios * ListaServicios :: buscar( string nom ){
        NodoServicios * iterador = getCab();
        while( iterador != NULL ){
            if( nom.compare( iterador -> getInfo() -> getNombre() ) == 0){
                return iterador -> getInfo();
            }   //fin del if
            iterador = iterador -> getSig();
        }   //fin del while
        return NULL; 
    }   //fin del m�todo buscar


    void ListaServicios :: desplegar( TComboBox * box){
        NodoServicios * iterador = getCab();
        box -> Clear();
        while( iterador != NULL ){
            box -> Items -> Add( iterador -> getInfo() -> getNombre().c_str() );
            iterador = iterador -> getSig();
        }   //fin del while
    }   //fin del m�todo desplegar
#pragma package(smart_init)
 