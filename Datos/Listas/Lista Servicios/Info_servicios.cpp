//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop
#include "Gestor.h"

#include "Info_servicios.h"
    InfoServicios :: InfoServicios( string nom, int cod ){
        cola = new ColaPrioridades();
        setNombre( nom );
        agentes = new ListaAgentes();
        atendidas = new ListaLlamadas();
        setCodigo( cod );

    }   //fin del constructor

    ColaPrioridades * InfoServicios :: getCola(){
        return cola;
    }   //fin del getCola

    void InfoServicios :: setNombre( string nom ){
        nombre = nom;
    }
    string InfoServicios :: getNombre(){
        return nombre;
    }

    ListaAgentes * InfoServicios :: getAgentes(){
        return agentes;
    }   //fin del getAgentes

    ListaLlamadas * InfoServicios :: getLlamadasAtendidas(){
        return atendidas;
    }
    void InfoServicios :: setCodigo( int c ){
        codigo = c;
    }
    int InfoServicios :: getCodigo(){
        return codigo;
    }

    string InfoServicios :: aString(){
        string msn = "";
        msn += "C�digo: ";
        msn += IntToStr( getCodigo() ).c_str();
        msn += "\nNombre del servicio: " + getNombre();
        return msn;
    }   //fin del m�todo InfoServicios
#pragma package(smart_init)
 