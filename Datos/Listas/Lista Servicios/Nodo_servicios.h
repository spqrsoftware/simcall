//---------------------------------------------------------------------------
#ifndef Nodo_serviciosH
#define Nodo_serviciosH
#include "Info_servicios.h"
class NodoServicios{
    private:
        NodoServicios * sig;
        NodoServicios * ant;
        InfoServicios * info;
    public:
        NodoServicios(InfoServicios * );
        void setSig( NodoServicios * );
        void setAnt( NodoServicios * );
        void setInfo( InfoServicios * );

        NodoServicios * getSig();
        NodoServicios * getAnt();
        InfoServicios * getInfo();
};  //fin de la clase NodoServicios
#endif
 