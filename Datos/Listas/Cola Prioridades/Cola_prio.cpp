//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include "Cola_prio.h"

ColaPrio :: ColaPrio(){
        setFrente( NULL );
        setFinal( NULL );
        size = 0;
    }   //fin del constructor
//*******************************  Sets y gets  ******************************//
    void ColaPrio :: setFrente( NodoColaPrioridades * f){
        frente = f;
    }
    void ColaPrio :: setFinal( NodoColaPrioridades * f){
        final = f;
    }
    NodoColaPrioridades * ColaPrio :: getFrente(){
        return frente;
    }
    NodoColaPrioridades * ColaPrio :: getFinal(){
        return final;
    }
    int ColaPrio :: getSize(){
        return size;
    }
//********************************  M�todos  *********************************//
    void  ColaPrio :: ensartar( InfoLlamada * temp){
        NodoColaPrioridades * t = new NodoColaPrioridades( temp );
        if( getFrente() == NULL ){
            setFrente( t );
            setFinal( t );
        }else{
            final -> setSig( t );
            t -> setAnt( final );
            setFinal( t );
        }   //fin del if...else
        size++;
    }   //fin del m�todo insertar
    
    InfoLlamada * ColaPrio :: eliminar(){
        if( getFrente() != NULL ){
            InfoLlamada * temp = getFrente() -> getInfo();
            if( getFrente() == getFinal() ){
                delete frente;
                setFrente( NULL );
                setFinal( NULL );
            }else{
                NodoColaPrioridades * temp2 = getFrente();
                temp2 -> getSig() -> setAnt( NULL );
                setFrente( temp2 -> getSig() );
                delete temp2;
            }   //fin del if...else
            size--;
            return temp;
        }   //fin del if
        return NULL;
    }   //fin del m�todo eliminar

    InfoLlamada * ColaPrio :: Gfrente(){
        if( getFrente() != NULL ){
            return getFrente() -> getInfo();
        }   //fin del if
        return NULL;
    }   //fin del m�todo frente

    InfoLlamada * ColaPrio :: eliminarFinal(){
        if( getFrente() != NULL ){
            InfoLlamada * temp = NULL;
            if( getFrente() == getFinal() ){
                temp = getFrente() -> getInfo();
                delete getFrente();
                setFrente( NULL );
                setFinal( NULL );
                size--;
                return temp;
            }else{
                temp = getFinal() -> getInfo();
                setFinal( getFinal() -> getAnt() );
                delete getFinal() -> getSig();
                getFinal() -> setSig( NULL );
                size--;
                return temp;
            }   //fin del if...else
        }   //fin del if
        return NULL;
    }   //fin del m�todo eliminarFinal

    bool ColaPrio :: isEmpty(){
        if( getFrente() == NULL ){
            return true;
        }else{
            return false;
        }   //fin del if...else
    }   //fin del m�todo isEmpty

    void ColaPrio :: desplegar( TRichEdit * area){
        int j = getSize();
        string msn = "";
        for( int i = j; i > 0; i-- ){
            InfoLlamada * temp = eliminar();
            msn += temp -> getLlamada() -> aString();
            msn += "\n";
            ensartar( temp );
        }   //fin del for
    }   //fin del m�todo desplegar
#pragma package(smart_init)
 