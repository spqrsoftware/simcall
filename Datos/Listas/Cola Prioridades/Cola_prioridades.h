//---------------------------------------------------------------------------
#ifndef Cola_prioridadesH
#define Cola_prioridadesH
#include "Cola_prio.h"
#include "Info_llamada.h"
class ColaPrioridades{
    private:
        ColaPrio * cola[3];
    public:
        ColaPrioridades();
        void insertar( Llamada * llamada, int prioridad );
        InfoLlamada * perderLlamada( int prioridad );
        InfoLlamada * atenderLlamada();
        void desplegar( int, TRichEdit * );
        bool isEmpty();
};  //fin de la clase ColaPrioPrioridades
#endif
 