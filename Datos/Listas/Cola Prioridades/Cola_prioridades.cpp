//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include "Cola_prioridades.h"

    ColaPrioridades :: ColaPrioridades(){
        cola[0] = new ColaPrio();
        cola[1] = new ColaPrio();
        cola[2] = new ColaPrio();
    }   //fin del constructor

    void ColaPrioridades :: insertar( Llamada * llamada, int prioridad ){
        InfoLlamada * temp = new InfoLlamada( llamada );
        switch( prioridad ){
            case 1:
                cola[0] -> ensartar( temp );
                break;
            case 2:
                cola[1] -> ensartar( temp );
                break;
            case 3:
                cola[2] -> ensartar( temp );
                break;
            default:
                break;
        }   //fin del swtich
    }   //fin del m�todo insertar

    InfoLlamada * ColaPrioridades :: perderLlamada( int prioridad ){
        switch( prioridad ){
            case 1:
                return cola[0] -> eliminarFinal();
            case 2:
                return cola[1] -> eliminarFinal();
            case 3:
                return cola[2] -> eliminarFinal();
            default:
                break;
        }   //fin del swtich
        return NULL;
    }   //fin del m�todo perderLlamada

    InfoLlamada * ColaPrioridades :: atenderLlamada(){
        if( cola[0] -> Gfrente() != NULL ){
            return cola[0] -> eliminar();
        }   //fin del if
        if( cola[1] -> Gfrente() != NULL ){
            return cola[1] -> eliminar();
        }   //fin del if
        if( cola[2] -> Gfrente() != NULL ){
            return cola[2] -> eliminar();
        }   //fin del if
        return NULL;
    }   //fin del m�todo atenderLlamada

    void ColaPrioridades :: desplegar( int col, TRichEdit * area ){
        switch( col ){
            case 1:
                cola[0] -> desplegar( area );
                break;
            case 2:
                cola[1] -> desplegar( area );
                break;
            case 3:
                cola[2] -> desplegar( area );
                break;
            default:
                area -> Text = "La cola de prioridad elejida, no existe";
                break;
        }   //fin del switch
    }   //fin del m�todo desplegar

    bool ColaPrioridades :: isEmpty(){
        return ( cola[0] -> isEmpty() && cola[1] -> isEmpty() && cola[2] -> isEmpty() );
    }   //fin del m�todo ColaPrioridades
#pragma package(smart_init)
 