//---------------------------------------------------------------------------
#ifndef Cola_prioH
#define Cola_prioH
#include "Nodo_cola_prioridades.h"
#include "Info_llamada.h"
class ColaPrio{
    private:
        NodoColaPrioridades * frente;
        NodoColaPrioridades * final;
        void setFrente( NodoColaPrioridades * );
        void setFinal( NodoColaPrioridades * );
        NodoColaPrioridades * getFrente();
        NodoColaPrioridades * getFinal();
        int size;
    public:
        ColaPrio();
        int getSize();
        void ensartar( InfoLlamada * );
        InfoLlamada * eliminar();
        InfoLlamada * eliminarFinal();
        InfoLlamada * Gfrente();
        bool isEmpty();
        void desplegar( TRichEdit * );
};  //fin de la clase ColaPrio
#endif
 