//---------------------------------------------------------------------------
#ifndef Nodo_cola_prioridadesH
#define Nodo_cola_prioridadesH
#include "Info_llamada.h"
class NodoColaPrioridades{
    private:
        InfoLlamada * info;
        NodoColaPrioridades * sig;
        NodoColaPrioridades * ant;
    public:
        NodoColaPrioridades();
        NodoColaPrioridades( InfoLlamada * i );
        void setSig( NodoColaPrioridades * );
        NodoColaPrioridades * getSig();
        void setAnt( NodoColaPrioridades * );
        NodoColaPrioridades * getAnt();
        void setInfo( InfoLlamada * );
        InfoLlamada * getInfo();
};  //fin de la clase NodoColaPrioridades
#endif
 