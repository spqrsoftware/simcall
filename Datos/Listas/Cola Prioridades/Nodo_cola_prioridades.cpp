//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include "Nodo_cola_prioridades.h"

    NodoColaPrioridades :: NodoColaPrioridades(){
        setSig( NULL );
        setAnt( NULL );
        info = NULL;
    }
    NodoColaPrioridades :: NodoColaPrioridades( InfoLlamada * i ){
        setSig( NULL );
        setAnt( NULL );
        info = i;
    }

//*****************************  Sets y gets  ********************************//
    void NodoColaPrioridades :: setSig( NodoColaPrioridades * siguiente ){
        sig = siguiente;
    }   //fin del m�todo setSig
    NodoColaPrioridades * NodoColaPrioridades :: getSig(){
        return sig;
    }   //fin del m�todo getSig
    void NodoColaPrioridades :: setAnt( NodoColaPrioridades * a){
        ant = a;
    }   //fin del setAnt
    NodoColaPrioridades * NodoColaPrioridades ::getAnt(){
        return ant;
    }
    void NodoColaPrioridades :: setInfo( InfoLlamada * i ){
        info = i;
    }
    InfoLlamada * NodoColaPrioridades :: getInfo(){
        return info;
    }
#pragma package(smart_init)
 