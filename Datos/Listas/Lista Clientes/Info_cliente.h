//---------------------------------------------------------------------------
#ifndef Info_clienteH
#define Info_clienteH
#include "CLIENTE.h"
class InfoCliente{
    private:
        Cliente * cliente;
    public:
        InfoCliente();
        InfoCliente( Cliente * );
        void setCliente( Cliente * );
        Cliente * getCliente();
};   //fin de la clase Info
#endif
 