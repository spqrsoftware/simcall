//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include "Lista_clientes.h"

ListaClientes :: ListaClientes(){
    setCab(NULL);
}

void ListaClientes :: setCab( NodoCliente * jupa){
    cab = jupa;
}   //fin del setCab


//*******************************  P�blicos  *********************************//
NodoCliente * ListaClientes :: getCab(){
    return cab;
}   //fin del getCab

void ListaClientes :: insertarAlInicio( InfoCliente * info ){
    NodoCliente * temp = new NodoCliente( info );
    temp -> setSig( getCab() );
    setCab( temp );
}   //fin del m�todo

void ListaClientes :: insertarAlFinal( InfoCliente * info ){
    NodoCliente * temp = new NodoCliente( info );
    if( getCab() == NULL ){
        insertarAlInicio( info );
    }else{
        NodoCliente * iterador = getCab();
        while( iterador -> getSig() != NULL ){
            iterador = iterador -> getSig();
        }   //fin del while
        iterador -> setSig( temp );
    }   //fin del if....else
}   //fin del m�todo

void ListaClientes :: borrarAlInicio(){//metodo para borrar al inicio
    NodoCliente * iterador = getCab();

    if(getCab()!=NULL){//si hay algo en la lista
        if(getCab()->getSig()==NULL){//si solo hay un elemento
            delete cab;
            cab=NULL;
        }// cierra IF si solo hay 1 elemento
        else{//else si hay mas de 1 elemento
            setCab(getCab()->getSig() );
            delete iterador;
            iterador=NULL;
        }//cierra else
    }//cierra IF si lista es vacia
}//fin de metodo eliminar al inicio

void ListaClientes :: borrarAlFinal(){//metodo que elimina del final

    if(getCab()!=NULL){//si la lista NO esta vacia
        if(getCab()->getSig()==NULL){//si solo hay 1 elemento
            delete cab;
            cab=NULL;
        }//cierra IF si hay solo 1 elemento
        else{//else si hay mas de 1 elemento
            NodoCliente * ant = getCab();
            NodoCliente * borrar = getCab()->getSig();
            while(borrar->getSig()!=NULL){
                ant=ant->getSig();
                borrar=borrar->getSig();

            }//cierra while
            ant->setSig(NULL);
            delete borrar;
            borrar=NULL;
        }//cierra ELSE
    }//cierra if si la lista esta vacia
}//fin metodo borrar al final

void ListaClientes ::despliegue( TRichEdit * area){
    if( cab != NULL ){
        NodoCliente * iterador = cab;
        string texto = "";
        while( iterador != NULL ){
            texto += iterador -> getDato() -> getCliente() -> aString();

            iterador = iterador -> getSig();
        }   //fin del while
        area -> Text = texto.c_str();
    }   //fin del if
}   //fin del m�todo despliegue

#pragma package(smart_init)
 