//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include "Nodo_cliente.h"

NodoCliente :: NodoCliente(){
    dato = NULL;
    sig = NULL;
}   //fin del constructor

NodoCliente :: NodoCliente( InfoCliente * x ){
    dato = x;
    sig = NULL;
}   //fin del constructor

void NodoCliente :: setDato( InfoCliente * x ){
    dato = x;
}

void NodoCliente :: setSig( NodoCliente * siguiente ){
    sig = siguiente;
}
InfoCliente * NodoCliente :: getDato(){
    return dato;
}

NodoCliente * NodoCliente :: getSig(){
    return sig;
}
#pragma package(smart_init)
 