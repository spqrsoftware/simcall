//---------------------------------------------------------------------------
#ifndef Nodo_clienteH
#define Nodo_clienteH
#include "CLIENTE.h"
#include "Info_cliente.h"

class NodoCliente{
    private:
        InfoCliente * dato;
        NodoCliente * ant;
        NodoCliente * sig;
    public:
        NodoCliente();
        NodoCliente( InfoCliente * x );

        void setDato( InfoCliente * x );
        void setAnt( NodoCliente * anterior );
        void setSig( NodoCliente * siguiente );

        InfoCliente * getDato();
        NodoCliente * getAnt();
        NodoCliente * getSig();
        
};  //fin de la clase Nodo
#endif
 