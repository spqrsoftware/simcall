//---------------------------------------------------------------------------
#ifndef Lista_clientesH
#define Lista_clientesH 
#include <Controls.hpp>
#include "Nodo_cliente.h"
#include "Info_cliente.h"
class ListaClientes{
    private:
        NodoCliente * cab;
        void setCab( NodoCliente * );

    public:
        NodoCliente * getCab();
        ListaClientes();
        void insertarAlInicio( InfoCliente * );
        void insertarAlFinal( InfoCliente * );
        void borrarAlInicio();
        void borrarAlFinal();
        void despliegue( TRichEdit * area);
};   //fin de la clase ListaSimple
#endif
 