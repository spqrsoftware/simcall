//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include "Info_cliente.h"

InfoCliente :: InfoCliente(){
    setCliente(NULL);
}   //fin del constructor

InfoCliente :: InfoCliente( Cliente * x ){
    setCliente(x);
}   //fin de constructor

void InfoCliente :: setCliente( Cliente * x ){
    cliente = x;
}   //fin del setInfo

Cliente * InfoCliente :: getCliente(){
    return cliente;
}   //fin del getInfo
#pragma package(smart_init)
