//---------------------------------------------------------------------------
#ifndef Info_agenteH
#define Info_agenteH
#include "AGENTE.h"

class InfoAgente{
    private:
        Agente * agente;
    public:
        InfoAgente();
        InfoAgente( Agente * );
        InfoAgente( int c, string nom, string ap1, string ap2, string puesto );
        Agente* getAgente();
        void setAgente( Agente *);

};  //fin de la clase InfoA
#endif
 