//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include "Info_agente.h"
#include "AGENTE.h"

//*******************************  Constructores  ****************************//
    InfoAgente :: InfoAgente(){
        setAgente( NULL );
    }   //fin del constructor default

    InfoAgente :: InfoAgente( Agente * ag ){
        setAgente(ag);
    }   //fin de constructor

//***************************  Gets/Sets  ************************************//
    Agente* InfoAgente :: getAgente(){
        return agente;
    }
    void InfoAgente :: setAgente( Agente * ag ){
        agente = ag;
    }
#pragma package(smart_init)
 