//---------------------------------------------------------------------------
#ifndef Nodo_agenteH
#define Nodo_agenteH
#include "Info_agente.h"

class NodoAgente{
    private:
        NodoAgente * sig;
        NodoAgente * ant;
        InfoAgente * info;
    public:
        NodoAgente();
        NodoAgente( InfoAgente * );
        void setSig( NodoAgente * );
        void setAnt( NodoAgente * );
        void setInfo( InfoAgente * );

        NodoAgente* getSig();
        NodoAgente * getAnt();
        InfoAgente * getInfo();
};  //fin de la clase NodoA
#endif
 