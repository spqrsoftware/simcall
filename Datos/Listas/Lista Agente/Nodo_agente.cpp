//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include "Nodo_agente.h"

//********************************  Constructores  ***************************//
    NodoAgente :: NodoAgente(){
        setSig( NULL );
        setAnt( NULL );
        setInfo( NULL );
    }   //fin del constructor
    NodoAgente :: NodoAgente( InfoAgente * i ){
        setSig( NULL );
        setAnt( NULL );
        setInfo( i );
    }   //fin de constructor

//*****************************  Sets y gets  ********************************//
    void NodoAgente :: setSig( NodoAgente * siguiente){
        sig = siguiente;
    }
    void NodoAgente :: setAnt( NodoAgente * anterior){
        ant = anterior;
    }
    void NodoAgente :: setInfo( InfoAgente * i){
        info = i;
    }

    NodoAgente * NodoAgente :: getSig(){
        return sig;
    }
    NodoAgente * NodoAgente :: getAnt(){
        return ant;
    }
    InfoAgente * NodoAgente :: getInfo(){
        return info;
    }
#pragma package(smart_init)
