//---------------------------------------------------------------------------
#ifndef Lista_agentesH
#define Lista_agentesH
#include "Info_agente.h"
#include "Nodo_agente.h"
#include <ComCtrls.hpp>
#include <string>

class ListaAgentes{
    private:
        NodoAgente * cab;
    public:
        ListaAgentes();
        NodoAgente * getCab();
        void setCab( NodoAgente * );

        void insertar( InfoAgente * );
        bool eliminar( int );
        void desplegar( TRichEdit * );
        Agente * buscar( int );
        void asignarAgentes( TComboBox * );

};
#endif
 