//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include "Lista_agentes.h"

//**************************   Constructores  ********************************//
    ListaAgentes :: ListaAgentes(){
        setCab( NULL );
    }
    NodoAgente * ListaAgentes ::  getCab(){
        return cab;
    }
    void ListaAgentes :: setCab( NodoAgente * cabeza){
        cab = cabeza;
    }

//*******************************  M�todos  **********************************//
    void ListaAgentes :: insertar( InfoAgente * info ){
        NodoAgente * temp = new NodoAgente( info );
        if( getCab() == NULL ){
            setCab( temp );
            getCab() -> setSig( getCab() );
            getCab() -> setAnt( getCab() );
        }else{
            NodoAgente * iterador = getCab();
            do{
                if( iterador -> getInfo() -> getAgente() -> getCodigo() >=
                        info -> getAgente() -> getCodigo() ){
                    if( iterador == getCab() ){
                        temp -> setSig( iterador );
                        temp -> setAnt( iterador -> getAnt() );
                        iterador -> getAnt() -> setSig( temp );
                        iterador -> setAnt( temp );
                        setCab( temp );
                        return;
                    }else{
                        temp -> setSig( iterador );
                        temp -> setAnt( iterador -> getAnt() );
                        iterador -> getAnt() -> setSig( temp );
                        iterador -> setAnt( temp );
                        return;
                    }   //fin del if...else
                }   //fin del if
                iterador = iterador -> getSig();
            }while( iterador != getCab());

            temp -> setSig( getCab() );
            temp -> setAnt( getCab() -> getAnt() );
            getCab() -> getAnt() -> setSig( temp );
            getCab() -> setAnt( temp );

        }   //fin del if...else
    }   //fin del m�todo insertar

    bool ListaAgentes :: eliminar( int cod ){
        if( getCab() != NULL ){
            NodoAgente * iterador = getCab();
            if( iterador -> getInfo() -> getAgente() -> getEstadoConexion() == false ){
                if( iterador -> getSig() == getCab() ){
                    if( cod == iterador -> getInfo() -> getAgente() -> getCodigo() ){
                        delete iterador;
                        iterador = NULL;
                        setCab( NULL );
                        return true;
                    }   //fin del if
                }   //fin del if
                do{
                    if( cod == iterador -> getInfo() -> getAgente() -> getCodigo() ){
                        if( iterador == getCab() ){
                            iterador -> getAnt() -> setSig( iterador -> getSig() );
                            iterador -> getSig() -> setAnt( iterador -> getAnt() );
                            setCab( iterador -> getSig() );
                            delete iterador;
                            iterador = NULL;
                        }else{
                            iterador -> getAnt() -> setSig( iterador -> getSig() );
                            iterador -> getSig() -> setAnt( iterador -> getAnt() );
                            delete iterador;
                            iterador = NULL;
                        }   //fin del if...else
                        return true;
                    }   //fin del if
                    iterador = iterador -> getSig();
                }while( iterador != getCab() );
                return false;
            }   //fin del if
        }   //fin del if
        return false;
    }   //fin del m�todo eliminar

    Agente * ListaAgentes :: buscar( int cod ){
        if( getCab() != NULL ){
            NodoAgente * iterador = getCab();
            do{
                if( cod == iterador -> getInfo() -> getAgente() -> getCodigo() ){
                    return iterador -> getInfo() -> getAgente();
                }   //fin del if
                iterador = iterador -> getSig();
            }while( iterador != getCab() );
        }   //fin del if...else
        return NULL;
    }   //fin del m�todo buscar

    void ListaAgentes :: desplegar( TRichEdit * area ){
        if( cab != NULL ){
            NodoAgente * iterador = cab;
            string texto = "";
            do{
                texto += iterador -> getInfo() ->  getAgente() -> aString() + "\n";
                iterador = iterador -> getSig();
            }while( iterador != getCab() );   //fin del while
            area -> Text = texto.c_str();
        }   //fin del if
    }   //fin del m�todo desplegar

    void ListaAgentes :: asignarAgentes( TComboBox * box ){
        NodoAgente * iterador = getCab();
        box -> Clear();
        do{
            string msn = "";
            msn += IntToStr(iterador -> getInfo() -> getAgente() -> getCodigo()).c_str();
            msn += ". ";
            msn += iterador -> getInfo() -> getAgente() -> getNombre();
            box -> Items -> Add( msn.c_str() );
            iterador = iterador -> getSig();
        }while( iterador != getCab() );   //fin del while
    }   //fin del m�todo asignarAgentes
#pragma package(smart_init)
 