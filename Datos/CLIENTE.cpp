//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include "CLIENTE.h"

    Cliente::Cliente(){
        setCodigo(0);
        setNombre("");
        setApellido1("");
        setApellido2("");
        setTelefono("");
    }

    Cliente::Cliente(int cod, string nom, string ap1, string ap2,
                     string tel){
        setCodigo(cod);
        setNombre(nom);
        setApellido1(ap1);
        setApellido2(ap2);
        setTelefono(tel);
    }

    void Cliente::setCodigo(int cod){
        codigo = cod;
    }

    void Cliente::setNombre(string nom){
        nombre = nom;
    }

    void Cliente::setApellido1(string ap1){
        apellido1 =  ap1;
    }

    void Cliente::setApellido2(string ap2){
        apellido2 = ap2;
    }

    void Cliente::setTelefono(string tel){
        telefono = tel;
    }

    int  Cliente::getCodigo(){
        return codigo;
    }

    string Cliente::getNombre(){
        return nombre;
    }

    string Cliente::getApellido1(){
        return apellido1;
    }

    string Cliente::getApellido2(){
        return apellido2;
    }

    string Cliente::getTelefono(){
        return telefono;
    }

    string Cliente::aString(){
        string msn = "";
        msn += "Codigo: ";
        msn += IntToStr(getCodigo()).c_str();
        msn += "\nNombre: ";
        msn.append(getNombre());
        msn += " ";
        msn.append(getApellido1());
        msn += " ";
        msn.append(getApellido2());
        msn += "\nTelefono: " + getTelefono();
        return msn;
    }

//---------------------------------------------------------------------------

#pragma package(smart_init)
