//---------------------------------------------------------------------------
#ifndef CLIENTEH
#define CLIENTEH
#include <string>
class Cliente{
    private:
        int codigo;
        string nombre;
        string apellido1;
        string apellido2;
        string telefono;
    public:
        Cliente();
        Cliente(int, string, string, string, string);

        void setCodigo(int);
        void setNombre(string);
        void setApellido1(string);
        void setApellido2(string);
        void setTelefono(string);

        int  getCodigo();
        string getNombre();
        string getApellido1();
        string getApellido2();
        string getTelefono();

        string aString();
};
#endif

