//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include "LLAMADA.h"

    Llamada::Llamada(){
        setCliente(NULL);
        setHoraIngreso(0, 0, 0);
        setHoraAtendido(0, 0, 0);
        setHoraFinLlamada(0, 0, 0);
        setCodigoCola(0);
        setCodigoAgente(0);
    }

    Llamada::Llamada(Cliente *cli, int hi, int mi, int si, int codCol){
        setCliente(cli);
        setHoraIngreso(hi, mi, si);
        setHoraAtendido(0, 0, 0);
        setHoraFinLlamada(0, 0, 0);
        setCodigoCola(codCol);
        setCodigoAgente(0);
    }

    void Llamada::setCliente(Cliente *cli){
        clien = cli;
    }

    void Llamada::setHoraIngreso(int hi, int mi, int si){
        horaIngreso.h = hi;
        horaIngreso.m = mi;
        horaIngreso.s = si;
    }

    void Llamada::setHoraAtendido(int ha, int ma, int sa){
        horaAtendido.h = ha;
        horaAtendido.m = ma;
        horaAtendido.s = sa;
    }

    void Llamada::setHoraFinLlamada(int hfl, int mfl, int sfl){
        horaFinLlamada.h = hfl;
        horaFinLlamada.m = mfl;
        horaFinLlamada.s = sfl;
    }

    void Llamada::setCodigoCola(int cod){
        codCola = cod;
    }

    void Llamada::setCodigoAgente(int cod){
        codAgente = cod;
    }

    Cliente *Llamada::getCliente(){
        return clien;
    }

    string Llamada::getHoraIngreso(){
        string msn = "";
        msn += IntToStr(horaIngreso.h).c_str();
        msn += ":";
        msn += IntToStr(horaIngreso.m).c_str();
        msn += ":";
        msn += IntToStr(horaIngreso.s).c_str();
        return msn;
    }

    string Llamada::getHoraAtendido(){
        string msn = "";
        msn += IntToStr(horaAtendido.h).c_str();
        msn += ":";
        msn += IntToStr(horaAtendido.m).c_str();
        msn += ":";
        msn += IntToStr(horaAtendido.s).c_str();
        return msn;
    }

    string Llamada::getHoraFinLlamada(){
        string msn = "";
        msn += IntToStr(horaFinLlamada.h).c_str();
        msn += ":";
        msn += IntToStr(horaFinLlamada.m).c_str();
        msn += ":";
        msn += IntToStr(horaFinLlamada.s).c_str();
        return msn;
    }

    int Llamada::getCodigoCola(){
        return codCola;
    }

    int Llamada::getCodigoAgente(){
        return codAgente;
    }

     string Llamada::calculaTiempoEspera(){
        string msn = "";
        int h = 0;
        int m = 0;
        int s = 0;

        if (getHoraAtendido().compare("0:0:0")){
            h = horaFinLlamada.h - horaIngreso.h;
            m = horaFinLlamada.m - horaIngreso.m;
            s = horaFinLlamada.s - horaIngreso.s;
        }else{
            h = horaAtendido.h - horaIngreso.h;
            m = horaAtendido.m - horaIngreso.m;
            s = horaAtendido.s - horaIngreso.s;
        }
        h = (h < 0)? 24+h : h;
        m = (m <= 0)? 60+m : m;
        s = (s <= 0)? 60+s : s;

        msn += IntToStr(h).c_str();
        msn += ":";
        msn += IntToStr(m).c_str();
        msn += ":";
        msn += IntToStr(s).c_str();
        return msn;
     }   //fin del m�todo calcularTiempoEspera

     string Llamada::calculaTiempoAtendido(){
        string msn = "";
        int h = 0;
        int m = 0;
        int s = 0;

        if (getHoraAtendido().compare("0:0:0")){
            return "0:0:0";
        }else{
            h = horaFinLlamada.h - horaAtendido.h;
            m = horaFinLlamada.m - horaAtendido.m;
            s = horaFinLlamada.s - horaAtendido.s;
        }
        h = (h < 0)? 24+h : h;
        m = (m <= 0)? 60+m : m;
        s = (s <= 0)? 60+s : s;

        msn += IntToStr(h).c_str();
        msn += ":";
        msn += IntToStr(m).c_str();
        msn += ":";
        msn += IntToStr(s).c_str();
        return msn;
     }   //fin del m�todo calcularTiempoEspera 

     string Llamada::calculaTiempoTotal(){
        string msn = "";
        int h = 0;
        int m = 0;
        int s = 0;

        h = horaFinLlamada.h - horaIngreso.h;
        m = horaFinLlamada.m - horaIngreso.m;
        s = horaFinLlamada.s - horaIngreso.s;
        
        h = (h < 0)? 24+h : h;
        m = (m <= 0)? 60+m : m;
        s = (s <= 0)? 60+s : s;

        msn += IntToStr(h).c_str();
        msn += ":";
        msn += IntToStr(m).c_str();
        msn += ":";
        msn += IntToStr(s).c_str();
        return msn;
     }   //fin del m�todo calcularTiempoEspera


    string Llamada::aString(){
        string msn = "Si sirve.\n";
        msn += "Hora de ingreso: ";
        msn += getHoraIngreso();
        return msn;
    } 

//---------------------------------------------------------------------------
#pragma package(smart_init)
