//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include "AGENTE.h"


//*******************************  Constructores  ****************************//
    Agente :: Agente(){
        setCodigo(0);
        setNombre("Por asignar");
        setApellido1( "Por asignar" );
        setApellido2( "Por asginar" );
        setPuesto("No tiene");
        setConexionCola("Sin conexion");
        estado = false;
        setEstadoConexion(false);
        llamada = NULL;
    }   //fin del constructor default

    Agente :: Agente(int c, string nom, string ap1, string ap2, string puesto){
        setCodigo(c);
        setNombre(nom);
        setApellido1( ap1 );
        setApellido2( ap2 );
        setPuesto(puesto);
        setConexionCola("Sin conexion");
        estado = false;
        setEstadoConexion(false);
        llamada = NULL;
    }   //fin de constructor

//*******************************  Sets  *************************************//

    void Agente :: setCodigo(int c ){
        codigo = c;
    }

    void Agente :: setNombre( string n){
        nombre = n;
    }

    void Agente :: setApellido1( string ap ){
        apellido1 = ap;
    }

    void Agente :: setApellido2( string ap ){
        apellido2 = ap;
    }

    void Agente :: setPuesto( string p ){
        puesto = p;
    }

    void Agente :: setConexionCola( string cc ){
        conexion_cola = cc;
    }

    void Agente :: cambiarEstado(){
        estado = estado? false : true;
    }

    void Agente :: setEstadoConexion(bool ex){
        estado_conexion = ex;
    }

    void Agente::setLlamada(InfoLlamada *lla){
        llamada = lla;
    }

//**********************************  Gets  **********************************//
    int Agente :: getCodigo(){
        return codigo;
    }

    string Agente :: getNombre(){
        return nombre;
    }

    string Agente :: getApellido1(){
        return apellido1;
    }

    string Agente :: getApellido2(){
        return apellido2;
    }

    string Agente :: getPuesto(){
        return puesto;
    }

    string Agente :: getConexionCola(){
        return conexion_cola;
    }

    bool Agente :: getEstado(){
        return estado;
    }

    bool Agente :: getEstadoConexion(){
        return estado_conexion;
    }

    InfoLlamada *Agente::getLlamada(){
        return llamada;
    }

    string Agente :: aString(){
        string msn = "";
        msn += "Codigo: ";
        msn += IntToStr(getCodigo()).c_str();
        msn += "\n";
        msn += "Nombre: " + getNombre();
        msn += " " + getApellido1();
        msn += " " + getApellido2();
        msn += "\n";
        msn += "Puesto: " + getPuesto() + "\n" + "Conectado a: " + getConexionCola();
        msn += "\nEstado: ";
        if( estado == false ){
            msn += "Inactivo\n";
        }else{
            msn += "Activo\n";
        }   //fin del if...else
        msn += "�Est� atendiendo una llamada?\n";
        if( getEstadoConexion() ){
            msn += "SI";
        }else{
            msn += "NO";
        }   //fin del if...else
        msn += "\n********************************************";
        return msn;
    }

#pragma package(smart_init)
 