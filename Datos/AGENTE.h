//---------------------------------------------------------------------------
#ifndef AGENTEH
#define AGENTEH
#include <string>
#include <conio.h>
#include "Info_Llamada.h"

class Agente{
    private:
        int codigo;
        string nombre;
        string apellido1;
        string apellido2;
        string puesto;
        bool estado;
        string conexion_cola;
        bool estado_conexion;
        InfoLlamada *llamada;
    public:
        Agente();
        Agente(int, string nom, string ap1, string ap2, string puesto);

        void setCodigo(int);
        void setNombre( string );
        void setApellido1( string );
        void setApellido2( string );
        void setPuesto( string );
        void setConexionCola( string );
        void cambiarEstado();
        void setEstadoConexion(bool);
        void setLlamada(InfoLlamada *);

        int getCodigo();
        string getNombre();
        string getApellido1();
        string getApellido2();
        string getPuesto();
        string getConexionCola();
        bool getEstado();
        bool getEstadoConexion();
        InfoLlamada *getLlamada();
        
        string aString();

};  //fin de la clase Agente
#endif
 