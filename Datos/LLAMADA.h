//---------------------------------------------------------------------------
#ifndef LLAMADAH
#define LLAMADAH
#include "CLIENTE.h"
#include <string>
class Llamada{
    private:
        struct Tiempo{
            int h;
            int m;
            int s;
        };
        Cliente *clien;
        Tiempo horaIngreso;
        Tiempo horaAtendido;
        Tiempo horaFinLlamada;
        int codCola;
        int codAgente;

    public:
        Llamada();
        Llamada(Cliente *, int, int, int, int);

        void setCliente(Cliente *);
        void setHoraIngreso(int, int, int);
        void setHoraAtendido(int, int, int);
        void setHoraFinLlamada(int, int, int);
        void setCodigoCola(int);
        void setCodigoAgente(int);

        Cliente *getCliente();
        string getHoraIngreso();
        string getHoraAtendido();
        string getHoraFinLlamada();
        int getCodigoCola();
        int getCodigoAgente();

        string calculaTiempoEspera(); 
        string calculaTiempoAtendido();
        string calculaTiempoTotal();

        string aString();

};
//---------------------------------------------------------------------------
#endif
 