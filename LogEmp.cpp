//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include "LogEmp.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
Tform_log *form_log;
//---------------------------------------------------------------------------
__fastcall Tform_log::Tform_log(TComponent* Owner)
    : TForm(Owner)
{
    text ->Text = "";
}
//---------------------------------------------------------------------------
void __fastcall Tform_log::Button1Click(TObject *Sender)
{
    Gestor :: asignarServicios( combo_servicio );
    Gestor :: asignarAgentes( combo_agente ); 
    Gestor :: asignarAgentes( combo_agente2 );
}
//---------------------------------------------------------------------------

void __fastcall Tform_log::Button2Click(TObject *Sender)
{
    string msn = "";
    string serv = combo_servicio->Items->Strings[combo_servicio->ItemIndex].c_str();
    char *ag = combo_agente->Items->Strings[combo_agente->ItemIndex].c_str();
    int i = 0;
    while (true){
        if (ag[i] == '.')
            break;
        char l = ag[i];
        msn += l;
        i++;
    }
    int cod = StrToInt(msn.c_str());
    text->Text = Gestor::loguearAgente(cod, serv).c_str();
}
//---------------------------------------------------------------------------

void __fastcall Tform_log::Button3Click(TObject *Sender)
{    
    string msn = "";
    char *ag = combo_agente->Items->Strings[combo_agente->ItemIndex].c_str();
    int i = 0;
    while (true){
        if (ag[i] == '.')
            break;
        char l = ag[i];
        msn += l;
        i++;
    }
    int cod = StrToInt(msn.c_str());
    text->Text = Gestor::desloguearAgente(cod).c_str();
}
//---------------------------------------------------------------------------

void __fastcall Tform_log::Button4Click(TObject *Sender)
{
    string msn = "";
    char *ag = combo_agente2->Items->Strings[combo_agente2->ItemIndex].c_str();
    int i = 0;
    while (true){
        if (ag[i] == '.')
            break;
        char l = ag[i];
        msn += l;
        i++;
    }
    int cod = StrToInt(msn.c_str());
    int h = StrToInt(txt_h->Text);
    int m = StrToInt(txt_m->Text);
    int s = StrToInt(txt_s->Text);
    text->Text = Gestor::contestar(cod, h, m, s).c_str();
}
//---------------------------------------------------------------------------

void __fastcall Tform_log::ColgarClick(TObject *Sender)
{
    string msn = "";
    char *ag = combo_agente2->Items->Strings[combo_agente2->ItemIndex].c_str();
    int i = 0;
    while (true){
        if (ag[i] == '.')
            break;
        char l = ag[i];
        msn += l;
        i++;
    }
    int cod = StrToInt(msn.c_str());
    int h = StrToInt(txt_h->Text);
    int m = StrToInt(txt_m->Text);
    int s = StrToInt(txt_s->Text);
    text->Text = Gestor::colgar(cod, h, m, s).c_str();
}
//---------------------------------------------------------------------------

void __fastcall Tform_log::Button5Click(TObject *Sender)
{
    int pos = combo_prio->ItemIndex;
    string msn = "";
    char *ag = combo_agente2->Items->Strings[combo_agente2->ItemIndex].c_str();
    int i = 0;
    while (true){
        if (ag[i] == '.')
            break;
        char l = ag[i];
        msn += l;
        i++;
    }
    int cod = StrToInt(msn.c_str());
    int h = StrToInt(txt_h->Text);
    int m = StrToInt(txt_m->Text);
    int s = StrToInt(txt_s->Text);
    text->Text = Gestor::perderLlamada(cod, h, m, s, pos).c_str();
}
//---------------------------------------------------------------------------

