//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include "RealizarLlamada.h"
#include "Gestor.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
Tform_llamada *form_llamada;
//---------------------------------------------------------------------------
__fastcall Tform_llamada::Tform_llamada(TComponent* Owner)
    : TForm(Owner){
}
//---------------------------------------------------------------------------
void __fastcall Tform_llamada::Button2Click(TObject *Sender)
{
    Gestor :: asignarServicios( box );
}
//---------------------------------------------------------------------------
void __fastcall Tform_llamada::Button1Click(TObject *Sender)
{
    int cod = Gestor :: buscarServicio(
        box ->Items->Strings[box->ItemIndex].c_str() ) -> getCodigo();

    Llamada * temp = new Llamada(new Cliente(),
        StrToInt( txt_h -> Text.c_str() ),
        StrToInt( txt_m -> Text.c_str() ),
        StrToInt( txt_s -> Text.c_str() ),
        cod );

    Gestor :: buscarServicio( box ->Items->Strings[box->ItemIndex].c_str() )
    -> getCola() -> insertar( temp, StrToInt( txt_prio -> Text.c_str() ) );
}
//---------------------------------------------------------------------------
